var headerList = context.getVariable("request.headers.names").toString();
var verb = context.getVariable("request.verb");

if(headerList.search(/accept/i)!== -1){
        
        var acceptValKvm = context.getVariable("validation_accept_headers");
        
        var allowedAcceptTypeval = acceptValKvm.split(",");
        var accept = context.getVariable("request.header.accept");
        if(accept)
        {
           accept = accept.toLowerCase();
           if(accept.search(";") != -1)
           {
               accept = accept.subString(0,accept.index(";"));
           }
           if(allowedAcceptTypeval.indexOf(accept) === -1)
           {
               context.setVariable("errorJSON","invalid_accept_header");
               throw "invalid_accept_header";
           }
        }
       
    
        
    }
