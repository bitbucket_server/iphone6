 function populateValues(apiData)
     {
         var Data = JSON.parse(apiData);
         if(Data)
         {
             for(var key in Data)
             {
                 if(Data.hasOwnProperty(key))
                 {
                     print("key.." +key.toLowerCase());
                     context.setVariable(key.toLowerCase(),Data[key]);
                 }
             }
         }
     }
 try{
     var incomingIP = context.getVariable("proxy.client.ip");
     var oauthStatusCodes = JSON.parse(context.getVariable("Oauth_Status_Codes"));
     var genericErrorStatusCodes = JSON.parse(context.getVariable("Generic_Error_Status_Codes"));
     var logVariables = JSON.parse(context.getVariable("Log_variables"));
     var httpSecurityHeaders = JSON.parse(context.getVariable("Http_Security_Headers"));
     var validateMaxValues = context.getVariable("Validate_Max_Values");
     var ValidateHeaders = context.getVariable("Validate_Headers");
     populateValues(ValidateHeaders);
     populateValues(validateMaxValues);
     
     if(oauthStatusCodes)
     {
          if(oauthStatusCodes['INAVLID_CLIENT_KEY'])
                 {
                     context.setVariable("invalid_apikey",JSON.stringify(oauthStatusCodes['INAVLID_CLIENT_KEY']));
                 }
     }
     
      if(genericErrorStatusCodes){
                 if(genericErrorStatusCodes['INAVLID_AUTHORIZATION_HEADER'])
                 {
                     context.setVariable("invalid_authorization_header",JSON.stringify(genericErrorStatusCodes['INAVLID_AUTHORIZATION_HEADER']));
                 }
				if(genericErrorStatusCodes['EXPIRED_ACCESS_TOKEN'])
                 {
                     context.setVariable("access_token_expired",JSON.stringify(genericErrorStatusCodes['EXPIRED_ACCESS_TOKEN']));
                 }      
                 if(genericErrorStatusCodes['INAVLID_ACEESS_TOKEN'])
                 {
                     context.setVariable("invalid_access_token",JSON.stringify(genericErrorStatusCodes['INAVLID_ACEESS_TOKEN']));
                 }
                 if(genericErrorStatusCodes['INAVLID_SCOPE_FOR_API'])
                 {
                     context.setVariable("invalid_scope_api",JSON.stringify(genericErrorStatusCodes['INAVLID_SCOPE_FOR_API']));
                 }
                 if(genericErrorStatusCodes['MISSING_REQUEST_HEADER'])
                 {
                     context.setVariable("missing_request_header",JSON.stringify(genericErrorStatusCodes['MISSING_REQUEST_HEADER']));
                 }
                 if(genericErrorStatusCodes['INVALID_JSON_FORMAT'])
                 {
                     context.setVariable("invalid_json_format",JSON.stringify(genericErrorStatusCodes['INVALID_JSON_FORMAT']));
                 }
                 if(genericErrorStatusCodes['INVALID_METHOD'])
                 {
                     context.setVariable("invalid_method",JSON.stringify(genericErrorStatusCodes['INVALID_METHOD']));
                 }
                 if(genericErrorStatusCodes['INVALID_ACCEPT_HEADER'])
                 {
                     context.setVariable("invalid_accept_header",JSON.stringify(genericErrorStatusCodes['INVALID_ACCEPT_HEADER']));
                 }
                 if(genericErrorStatusCodes['INVALID_CONTENT_TYPE_HEADER'])
                 {
                     context.setVariable("invalid_content_type_header",JSON.stringify(genericErrorStatusCodes['INVALID_CONTENT_TYPE_HEADER']));
                 }
                 if(genericErrorStatusCodes['LARGE_PAYLOAD'])
                 {
                     context.setVariable("large_payload",JSON.stringify(genericErrorStatusCodes['LARGE_PAYLOAD']));
                 }
                 if(genericErrorStatusCodes['LARGE_URL'])
                 {
                     context.setVariable("large_url",JSON.stringify(genericErrorStatusCodes['LARGE_URL']));
                 }
                 if(genericErrorStatusCodes['LARGE_HEADER_VALUE'])
                 {
                     context.setVariable("large_header_value",JSON.stringify(genericErrorStatusCodes['LARGE_HEADER_VALUE']));
                 }
                 if(genericErrorStatusCodes['RESOURCE_NOT_FOUND'])
                 {
                     context.setVariable("resource_not_found",JSON.stringify(genericErrorStatusCodes['RESOURCE_NOT_FOUND']));
                 }
             
                 if(genericErrorStatusCodes['INCORRECT_PROTOCOL'])
                 {
                     context.setVariable("invalid_protocol",JSON.stringify(genericErrorStatusCodes['INCORRECT_PROTOCOL']));
                 }             
                 if(genericErrorStatusCodes['INTERNAL_SERVER_ERROR'])
                 {
                     context.setVariable("internal_server_error",JSON.stringify(genericErrorStatusCodes['INTERNAL_SERVER_ERROR']));
                 }
                 if(genericErrorStatusCodes['SPIKE_ARREST_VIOLATION'])
                 {
                     context.setVariable("spike_arrest_violation",JSON.stringify(genericErrorStatusCodes['SPIKE_ARREST_VIOLATION']));
                 }
                 if(genericErrorStatusCodes['MISSING_MANDATORY_PARAMETER'])
                 {
                     context.setVariable("missing_mandatory_parameter",JSON.stringify(genericErrorStatusCodes['MISSING_MANDATORY_PARAMETER']));
                 }
				 if(genericErrorStatusCodes['INVALID_ORIGIN'])
                 {
                     context.setVariable("invalid_origin",JSON.stringify(genericErrorStatusCodes['INVALID_ORIGIN']));
                 }
                 if(genericErrorStatusCodes['QUOTA_LIMIT_REACHED'])
                 {
                     context.setVariable("quota_limit_reached",JSON.stringify(genericErrorStatusCodes['QUOTA_LIMIT_REACHED']));
                 }
                  if(genericErrorStatusCodes['INVALID_IP'])
                 {
                     context.setVariable("invalid_ip",JSON.stringify(genericErrorStatusCodes['INVALID_IP']));
                 }
			} 
             
     
 }
 catch(err)
 {
     throw err;
 }