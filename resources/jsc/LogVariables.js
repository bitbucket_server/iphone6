 try {
    var host      = context.getVariable("request.header.host");
    var requestURL    = context.getVariable("request.uri");
    
    // set the timestamp
   var dt = new Date().getTime();
	//var timestamp = dt.format("DD/MM/YYYY HH:mm:ss");
	
	// set the headers
    var headers = {"Content-Type":"application/json"};
    
    var responseStatusCode = context.getVariable("message.status.code");
    var responseStatus ="";
    if(responseStatusCode == 502 || responseStatusCode == 503)
    {
        responseStatus = "timeout";
    }else if(responseStatusCode >= 200 || responseStatusCode < 400)
    {
        responseStatus = "ok";
    }else if(responseStatusCode >=  400 || responseStatusCode < 500)
    {
        responseStatus = "error";
    } else if(responseStatusCode >= 502 )
    {
        responseStatus = "failure";
    }
    
    // set the error attributes
    var payload = {
    "transactionId": context.getVariable("transactionId"),
    "host": context.getVariable("request.header.host"),
    "organization": context.getVariable("organization.name"),
    "environment": context.getVariable("environment.name"),
    "timestamp": dt,
    "proxy": context.getVariable("apiproxy.name"),
    "basePath": context.getVariable("proxy.basepath"),
    "resourcePath": context.getVariable("request.uri"),
    "flowName": context.getVariable("current.flow.name"),
    "requestURL": context.getVariable("client.scheme") + "://" + host + requestURL,
    "method": context.getVariable("request.verb"),
    "requestPayload": context.getVariable("request.content"),
    "responseStatus" : responseStatus,
    "statusCode": context.getVariable("statusCode"),
    "errorSource": context.getVariable("errorSource"),
    "errorCode": context.getVariable("errorCode"),
    "errorDescription": context.getVariable("description")
    };
    
   /* // define URL
    var url = APEDG + '/trigErrEmailAlerts';
    // set the request
    var req = new Request(url, "POST", headers, JSON.stringify(payload));
    // trigger the asynchronous call
    var errObject = httpClient.send(req);*/
} catch (err) {
    context.setVariable("logVariables",JSON.stringify(payload));
    throw err;
} 